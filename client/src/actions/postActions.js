import axios from "axios";

import {
  ADD_POST,
  GET_ERRORS,
  GET_POSTS,
  POST_LOADING,
  GET_POST,
  DELETE_POST,
  LIKE_POST,
  DELETE_COMMENT,
  CLEAR_ERRORS
} from "./types";

/* I'm not sure if this is better than the asynchronous way,
   but it allows me to clear the textfield upon response.
   If for some reason the asynch way is better there is a 
   commit where it is done that way. Potentially you could
   add a variable postSuccess to the store (post reducer) 
   and update it the same way that clearErrors is updated. 
*/
// Add Post
export const addPost = postData => async dispatch => {
  let PostSuccess = null;
  try {
    const res = await axios.post("/api/posts", postData);
    PostSuccess = true;
    dispatch({
      type: ADD_POST,
      payload: res.data
    });
    dispatch(clearErrors());
  } catch (err) {
    PostSuccess = false;
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
  return PostSuccess;
};

// Add Post
export const addComment = (postId, commentData) => async dispatch => {
  let CommentSuccess = null;
  try {
    const res = await axios.post(`/api/posts/comment/${postId}`, commentData);
    CommentSuccess = true;
    dispatch({
      type: GET_POST,
      payload: res.data
    });
    dispatch(clearErrors());
  } catch (err) {
    CommentSuccess = false;
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
  return CommentSuccess;
};

// Get Posts
export const getPosts = () => dispatch => {
  dispatch(setPostLoading);
  axios
    .get("/api/posts")
    .then(res =>
      dispatch({
        type: GET_POSTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_POSTS,
        payload: null
      })
    );
};

// Get Post
export const getPost = id => dispatch => {
  dispatch(setPostLoading);
  axios
    .get(`/api/posts/${id}`)
    .then(res =>
      dispatch({
        type: GET_POST,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_POST,
        payload: null
      })
    );
};

// Delete Post
export const deleteComment = (postId, commentId) => dispatch => {
  axios
    .delete(`/api/posts/comment/${postId}/${commentId}`)
    .then(res =>
      dispatch({
        type: DELETE_COMMENT,
        payload: commentId
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Post
export const deletePost = id => dispatch => {
  axios
    .delete(`/api/posts/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_POST,
        payload: id
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//Add Like
export const addLike = id => dispatch => {
  axios
    .post(`/api/posts/like/${id}`)
    .then(({ data }) => {
      dispatch({
        type: LIKE_POST,
        payload: data //pass in updated post
      });
    })
    .catch(({ response }) => {
      dispatch({
        type: GET_ERRORS,
        payload: response.data
      });
    });
};

// // Add Like
// export const addLike = id => dispatch => {
//   axios
//     .post(`/api/posts/like/${id}`)
//     .then(res => dispatch(getPosts()))
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

//Remove Like
export const removeLike = id => dispatch => {
  axios
    .post(`/api/posts/unlike/${id}`)
    .then(({ data }) => {
      dispatch({
        type: LIKE_POST,
        payload: data //pass in updated post
      });
    })
    .catch(({ response }) => {
      dispatch({
        type: GET_ERRORS,
        payload: response.data
      });
    });
};

// // Remove Like
// export const removeLike = id => dispatch => {
//   axios
//     .post(`/api/posts/unlike/${id}`)
//     .then(res => dispatch(getPosts()))
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

// Set loading state
export const setPostLoading = () => {
  return {
    type: POST_LOADING
  };
};

// // Clear errors
// export const clearErrors = () => {
//   return {
//     type: GET_ERRORS,
//     payload: {}
//   };
// };

// Maybe a clener way of doing this is dispatching its
// own type to the errorReducer. It has the same effect.
// It is probably more accessable. You may want to add
// this to the other forms.
// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
