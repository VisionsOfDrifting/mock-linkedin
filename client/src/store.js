import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import rootReducer from "./reducers";

const initialState = {};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

/* You need to implement createStore with 
rootReducer, initial state, 
compose(applyMiddleware(), window.REDUX_DEVTOOLS_EXTENSION...) 
In order to get the dev tools to actually display.
*/

export default store;
